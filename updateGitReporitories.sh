echo "updating..."
for f in *; do
	if [[ -d $f ]]; then
		echo $f"..."
		cd $f
		git pull
		cd ..
	fi
done
echo "complete"
