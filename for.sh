echo "Demo for..."
for i in *; do
	echo "$i"
done

echo "Demo while..."
ls -1 | while read fn; do
	echo "$fn"
done
