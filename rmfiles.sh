#!/bin/bash

if test $1 && test $2; then
	saveIFS=$IFS
	IFS=$(echo -en "\n\b")
	fileArray=(`find $1 -iname $2`)
	tLen=${#fileArray[@]}
	for ((i=0; i<${tLen}; i++)); do
		file=${fileArray[$i]}
		echo "removing $file..."
		rm -rf $file
	done
	IFS=$saveIFS
else
	echo "Usage: rmfiles.sh <directory> <pattern file name>"
fi
