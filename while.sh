secretNumber=$(($RANDOM % 100 + 1))
guess=-1
while test "$guess" != "$secretNumber"; do
	echo -n "I am thinking of a number between 1 and 100. Enter your guess:"
	read guess
	if test "$guess" = ""; then
		echo "Please enter a number!"
	elif test "$guest" = "$secretNumber"; then
		echo -e "\aYes! $guess is the correct answer!"
	elif test "$secretNumber" -gt "$guess"; then
		echo "The secret number is larger than your guess. Try again."
	else
		echo "The secret number is smaller than your guess. Try again."
	fi
done

echo "Secret number is $secretNumber."

n=1
while test $n -le 6; do
	echo $n
	let	n++
	let n=n+1
done

y=1
while test $y -le 12; do
	x=1
	while test $x -le 12; do
		printf "% 4d" $(($x + $y))
		let x++
	done
	echo 
	let y++
done
